<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationVilldest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('villdest', function (Blueprint $table) {
            $table->increments('id'); //,55= tambah panjang data. 
            $table->string('username'); //->unique(); supaya nama file tdk dpt digunakan lagi.
            $table->string('password');
            $table->rememberToken(); //menyimpan bekas session
            $table->timestamps();//membuat create field,add create.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() //untuk rollback
    {
        Schema::drop('villdest');
    }
}
