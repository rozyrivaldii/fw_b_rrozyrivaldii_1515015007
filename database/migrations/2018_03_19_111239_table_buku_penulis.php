<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableBukuPenulis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku_penulis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penulis_id')->unsigned();
            $table->foreign('penulis_id')
                    ->references('id')
                    ->on('penulis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->integer('buku_id')->unsigned();
            $table->foreign('buku_id')
                    ->references('id')
                    ->on('buku')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku_penulis');
    }
}
