<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTableBukuPembeli extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bukupembeli', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('pembeli_id',false,true);
			$table->foreign('pembeli_id')->references('id')->on('pembeli');
			$table->integer('buku_id',false,true);
			$table->foreign('buku_id')->references('id')->on('buku');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bukupembeli');
    }
}
