<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTableBukuPenulis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bukupenulis', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('penulis_id',false,true);
			$table->foreign('penulis_id')->references('id')->on('penulis');
			$table->integer('buku_id',false,true);
			$table->foreign('buku_id')->references('id')->on('buku');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bukupenulis');
    }
}
