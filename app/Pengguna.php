<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengguna extends Authenticatable
{
	protected $table = 'pengguna';
	protected $fillable = [
		'username','password',
	];

  public function Admin(){
		return $this->hasOne('App\Admin');
	}

	public function pembeli(){
    return $this->belongsTo('App\Pembeli');
  }
}
