<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use Session;

class isAdmin
{
    private $auth;
    public function __construct(){
        $this->auth = app('auth');
    }
    public function handle($request, Closure $next)
    {
      if(Auth::check()){
        if (Auth::user()->level == "admin") {
          return $next($request);
        }
        Session::flash('informasi','Anda Tidak Memiliki Hak Akses');
        return redirect()->back();
      }
      return redirect('login')
      ->withErrors('Anda harus login terlebih dahulu');
    }
}
