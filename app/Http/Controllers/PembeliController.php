<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\pembeli;

class PembeliController extends Controller
{
    public funtion awal(){
		$pembeli = pembeli::all();
		return view('pembeli.app',compact('pembeli'));
	}

	public funtion tambah(){
		return view('pembeli.tambah');
	}

	public funtion simpan(Request $input){
		$pembeli = new pembeli();
		$pembeli->nama   = $input->nama;
		$pembeli->notlp  = $input->notlp;
		$pembeli->email  = $input->email;
		$pembeli->alamat = $input->alamat;
		$status = $pembeli->save();
		return redirect('pembeli')->with(['status'=>$status]);
	}

	public funtion edit($id){
		$pembeli = pembeli::find($id);
		return view('pembeli.edit')->with(array('pembeli'=>$pembeli));
	}

	public funtion update($id, Request $input){
		$pembeli = pembeli::find($id);
		$pembeli->nama   = $input->nama;
		$pembeli->notlp  = $input->notlp;
		$pembeli->email  = $input->email;
		$pembeli->alamat = $input->alamat;
		$status = $pembeli->save(); 
		return redirect('pembeli')->with(['status'=>$status]);
	}

	public funtion hapus($id){
		$pembeli = pembeli::find($id);
		$pembeli->delete();
		return redirect('pembeli');
	}
}
