<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tabel_Kategori;

class KategoriController extends Controller
{
    public function awal(){
        $kategori=Tabel_Kategori::all();
        return view('kategori.app', compact('kategori'));
    }
    public function tambah(){
    return view ('kategori.tambah');

    }
    public function simpan(Request $input){
    	$kategori = new Tabel_Kategori();
    	$kategori->deskripsi = $input -> deskripsi;
    	$kategori->save();
    	return redirect ('kategori');
    }

    public function edit($id){
    	$kategori = Tabel_Kategori::find($id);
    	return view ('kategori.edit')->with(array('kategori'=>$kategori));
    }

    public function update ($id, Request $input){
    	$kategori = Tabel_Kategori::find($id);
    	$kategori->deskripsi = $input->deskripsi;
    	$status = $kategori-> save();
    	return redirect ('kategori')->with(['status'=>$status]);
    }

    public function hapus($id){
    	$kategori = Tabel_Kategori::find($id);
    	$kategori->delete();
    	return redirect('kategori');
    }
}
