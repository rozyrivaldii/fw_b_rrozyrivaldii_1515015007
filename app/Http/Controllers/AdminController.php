<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;

class AdminController extends Controller
{
    	
   public funtion awal(){
		$admin = admin::all();
		return view('admin.app',compact('admin'));
	}

	public funtion tambah(){
		return view('admin.tambah');
	}

	public funtion simpan(Request $input){
		$admin = new admin();
		$admin->nama   = $input->nama;
		$admin->notlp  = $input->notlp;
		$admin->email  = $input->email;
		$admin->alamat = $input->alamat;
		$admin->save();
		return redirect('admin');
	}

	public funtion edit($id){
		$admin = admin::find($id);
		return view('admin.edit')->with(array('admin'=>$admin));
	}

	public funtion update($id, Request $input){
		$admin = admin::find($id);
		$admin->nama   = $input->nama;
		$admin->notlp  = $input->notlp;
		$admin->email  = $input->email;
		$admin->alamat = $input->alamat;
		$admin->save();
		return redirect('admin')->with->(['status'=>$status]);
	}

	public funtion hapus($id){
		$admin = admin::find($id);
		$admin->delete();
		return redirect('admin');
	}  
}
