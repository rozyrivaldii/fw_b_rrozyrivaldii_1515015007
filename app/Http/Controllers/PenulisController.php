<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tabel_Penulis;

class PenulisController extends Controller
{
   public function awal(){
		$penulis = Tabel_Penulis::all();
		return view('penulis.app',compact('penulis'));
	}

	public function tambah(){
		return view('penulis.tambah');
	}

	public function simpan(Request $input){
		$penulis = new Tabel_Penulis();
		$penulis->nama   = $input->nama;
		$penulis->notelp  = $input->notlp;
		$penulis->email  = $input->email;
		$penulis->alamat = $input->alamat;
		$status = $penulis->save();
		return redirect('penulis')->with(['status'=>$status]);
	}

	public function edit($id){
		$penulis = Tabel_Penulis::find($id);
		return view('penulis.edit')->with(array('penulis'=>$penulis));
	}

	public function update($id, Request $input){
		$penulis = Tabel_Penulis::find($id);
		$penulis->nama   = $input->nama;
		$penulis->notelp  = $input->notlp;
		$penulis->email  = $input->email;
		$penulis->alamat = $input->alamat;
		$status = $penulis->save();
		return redirect('penulis')->with(['status'=>$status]);
	}

	public function hapus($id){
		$penulis = Tabel_Penulis::find($id);
		$penulis->delete();
		return redirect('penulis');
	}
}
