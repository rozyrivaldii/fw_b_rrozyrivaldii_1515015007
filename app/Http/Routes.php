<?
Route::get('/buku',"BukuController@awal");
Route::get('/tambah',"BukuController@lihat");

Route::get('/tambah/penulis',"PenulisController@tambah");
Route::post('/penulis/simpan',"PenulisController@simpan");
Route::get('/penulis',"PenulisController@awal");
Route::get('/penulis/edit/{penulis}',"PenulisController@edit");
Route::post('/penulis/update/{penulis}',"PenulisController@update");
Route::get('/penulis/hapus/{penulis}',"PenulisController@hapus");

Route::get('/tambah/buku',"BukuController@tambah");
Route::post('/buku/simpan',"BukuController@simpan");

Route::get('/kategori',"KategoriController@tambah");
Route::post('/kategori/simpan',"KategoriController@simpan");
Route::get('/kategori/edit/{kategori}',"KategoriController@edit");
Route::post('/kategori/update/{kategori}',"KategoriController@update");
Route::get('/kategori/hapus/{kategori}',"KategoriController@hapus");

Route::get('/pembeli/tambah',"PembeliController@tambah");
Route::post('/pembeli/simpan',"PembeliController@simpan");
Route::get('/pembeli',"PembeliController@awal");
Route::get('/pembeli/edit/{pembeli}',"PembeliController@edit");
Route::post('/pembeli/update/{pembeli}',"PembeliController@update");
Route::get('/pembeli/hapus/{pembeli}',"PembeliController@hapus");

Route::get('/admin',"AdminController@awal");
Route::get('/admin/tambah',"AdminController@tambah");
Route::post('/admin/simpan',"AdminController@simpan");
Route::get('/admin/lihat',"AdminController@lihat");
Route::get('/admin/edit/{admin}',"AdminController@edit");
Route::post('/admin/update/{admin}',"AdminController@update");
Route::get('/admin/hapus/{admin}',"AdminController@hapus");


>