<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table = 'pembeli';



	public function getUsernameAttribute(){
		return $this->pengguna->username;
	}

	public function getPasswordAttribute(){
		return $this->pengguna->password;
	}

  public function pengguna(){
    return $this->hasOne('App\Pengguna');
  }
}
