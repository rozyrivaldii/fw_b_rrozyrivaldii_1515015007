<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabelBukuPembeli extends Model
{
    protected $table = 'buku_pembeli';
    protected $fillable = ['username','password',];
    protected $hidden = [ 'password','remember_token',];
}
