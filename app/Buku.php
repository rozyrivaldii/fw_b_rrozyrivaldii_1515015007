<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Penulis;

class Buku extends Model
{
    protected $table = 'buku';

	public function kategori(){
		return $this->belongsTo('App\Kategori');
	}

	public function penulis(){
		return $this->belongsToMany(Penulis::class)
                ->withPivot('id')
                ->withTimestamps();
	}
}
