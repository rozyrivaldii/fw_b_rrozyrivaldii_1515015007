<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabel_admin extends Model
{
    protected $table = 'admin';
    protected $fillable = ['username','password',];
    protected $hidden = [ 'password','remember_token',];
}
