<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class villdest extends Model
{
    protected $table = 'villdest';
    protected $fillable = ['username','password',];
    protected $hidden = [ 'password','remember_token',];
}
