<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabelBukuPenulis extends Model
{
    protected $table = 'buku_penulis';
    protected $fillable = ['username','password',];
    protected $hidden = [ 'password','remember_token',];
}
