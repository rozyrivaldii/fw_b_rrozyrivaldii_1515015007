<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabel_Kategori extends Model
{
    protected $table = 'Kategori';
    protected $fillable = ['username','password',];
    protected $hidden = [ 'password','remember_token',];
}
