<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabel_Buku extends Model
{
    protected $table = 'buku';
    protected $fillable = ['username','password',];
    protected $hidden = [ 'password','remember_token',];
}
