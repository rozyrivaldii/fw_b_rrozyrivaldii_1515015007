<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buku;

class Penulis extends Model
{
	protected $table = 'penulis';
	protected $fillable = ['nama','notlp','email','alamat'];
	
    public function buku(){
		return $this->belongsToMany(Buku::class);
	}
}
