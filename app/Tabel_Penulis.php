<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabel_Penulis extends Model
{
    protected $table = 'penulis';
    protected $fillable = ['username','password',];
    protected $hidden = [ 'password','remember_token',];
}
