<?
namespace App;
use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model{
	protected $table = 'pengguna';
	protected $filable = ['username','password',];
	protected $hidden = ['password','remember_token',];

	public function Pembeli(){
		return $this->hasOne(Pembeli::class);
	}
}
