<div class="form-group">
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

	<div class="col-sm-5">
		{!! Form::Label('Judul') !!}
		{!! Form::text('judul',null,['class'=>'form-control','placeholder'=>"Judul"]) !!}
	</div>

	<div class="col-sm-5">
		{!! Form::Label('penulis', 'Pilih Penulis') !!}
		{!! Form::select('penulis', $author, null, ['class' => 'form-control']) !!}

	</div>

	<div class="col-sm-5">
		{!! Form::Label('kategori', 'Pilih Kategori') !!}
		{!! Form::select('kategori', $categories, null, ['class' => 'form-control']) !!}

	</div>

	<div class="col-sm-5">
		{!! Form::Label('penerbit', 'Penerbit') !!}
		{!! Form::text('penerbit',null,['class'=>'form-control','placeholder'=>"Penerbit"]) !!}
	</div>

	<div class="col-sm-5">
		{!! Form::Label('tanggal', 'Tanggal Rilis') !!}
		{!! Form::date('tanggal',null,['class'=>'form-control']) !!}
	</div>
	</center>

</div>
