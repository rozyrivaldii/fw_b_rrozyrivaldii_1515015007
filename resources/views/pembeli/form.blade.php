<div class="form-group">
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
    @endif
</div>
	<div class="form-group">
		<label class="col-md-2">Nama</label>
		<div class="col-sm-9">
			{!! Form::text('nama',null,['class'=>'form-control','placeholder'=>"Nama"]) !!}
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2">No. Telepon</label>
		<div class="col-sm-9">
			{!! Form::text('notlp',null,['class'=>'form-control','placeholder'=>"No Telepon"]) !!}
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2">E-mail</label>
		<div class="col-sm-9">
			{!! Form::text('email',null,['class'=>'form-control','placeholder'=>"Email"]) !!}
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2">Alamat</label>
		<div class="col-sm-9">
			{!! Form::text('alamat',null,['class'=>'form-control','placeholder'=>"Alamat"]) !!}
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2">Username</label>
		<div class="col-sm-9">
			{!! Form::text('username',null,['class'=>'form-control','placeholder'=>"Username"]) !!}
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2">Password</label>
		<div class="col-sm-9">
			{!! Form::text('password',null,['class'=>'form-control','placeholder'=>"Password"]) !!}
		</div>
	</div>

