@extends('master')
@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-heading">
               <strong>Data Pembeli</strong>
               <div class="pull-right">
                   Tambah Data <a href="pembeli/tambah"><img src="{{ asset('add.ico') }}" height="20"></img></a>
               </div>
            </div>
            <div class="panel-body">
                <table class="table">
                {{ $status or ' ' }}
                <tr>
                    <td>  Nama        </td>
                    <td>  No Telepon  </td>
                    <td>  Email       </td>
                    <td>  Alamat      </td>
                </tr>
                @foreach($pembeli as $Pembeli)
                    
                <tr>
                    <td>{{ $Pembeli->nama }}</td>
                    <td>{{ $Pembeli->notlp}}</td>
                    <td>{{ $Pembeli->email }}</td>
                    <td>{{ $Pembeli->alamat}}</td>
                    <td>
                        <a href="{{url('pembeli/edit/'.$Pembeli->id)}}"><img src="{{ asset('edit.png') }}" height="20"></img></a>
                        <a href="{{url('pembeli/hapus/'.$Pembeli->id)}}"><img src="{{ asset('delete.png') }}" height="20"></img></a>
                    </td>
                </tr>
                @endforeach
            </table>
            </div>
        </div>
    </div>
</div>
@endsection