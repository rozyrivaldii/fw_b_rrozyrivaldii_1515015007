@extends('master')
@section('content')
{{ $status or ' ' }}
		<div class="panel panel-info">
			<div class="panel-heading">
				Kategori
				<div class="pull-right">
				Tambah Kategori <a href="{{ url('tambah/kategori')}}"><img src="{{asset('add.ico')}}" height="20></img></a">
				</div>
			</div>
			<div class="panel-body">
				<table class="table">
					<tr>
						<td><b>Deskirpsi</b></td>
					</tr>
					@foreach($kategori as $kategori)
					<tr>
						<td>{{ kategori-> deskripsi }} </td>
						<td>
							<a href="{{url ('kategori/edit/'.$kategori->id)}}"></a>
							<a href="{{url ('kategori/hapus/'.$kategori->id)}}"></a>
						</td>
					</tr>
					@foreach
				</table>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-primary">
						<div class="panel-heading">
							Tambah Kategori
						</div>
					<div class="panel-body">
						{!! Form::open(['url'=>'kategori/simpan','class'=>'form-horizontal']) !!}
						@include('buku.form')
						<div style="width:100%;text-align:center;">
							<button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
							<input type="button" value="Reset" class="btn btn-danger" onClick="window.location.reload()"/>
						</div>

					</div>
					
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	</div>
</div>
@endsection